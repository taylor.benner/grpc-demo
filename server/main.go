package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "grpc-demo/example"

	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// server is used to implement example.ExampleServer
type exampleServer struct {
	pb.UnimplementedExampleServer
}

// Get implements example.Get
// You can implement this however your business logic needs
// Go get stuff from databases, process data, make other request, ect
func (s *exampleServer) Get(ctx context.Context, request *pb.GetRequest) (*pb.GetResponse, error) {
	log.Printf("Got Request for ID: %s\n", request.GetId())
	return &pb.GetResponse{Id: request.GetId(), Message: "Hello Get!"}, nil
}

func main() {
	// Create tcp listener on port 50051
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create server instance and register it as a gRPC server
	server := grpc.NewServer()
	pb.RegisterExampleServer(server, &exampleServer{})

	// Serve gRPC server over tcp listener
	fmt.Printf("Starting gRPC server on port %s...\n", port)
	if err := server.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
