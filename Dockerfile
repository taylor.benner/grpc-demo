############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/grpc-demo
COPY . .

# Fetch dependencies.
# Using go mod.
RUN go mod download
RUN go mod verify

# Static build required so that we can safely copy the binary over.
RUN CGO_ENABLED=0 \
    go install \
    -ldflags '-extldflags "-static"' \
    grpc-demo/server

    
############################
# STEP 2 build a small image
############################
FROM scratch

# Copy our static executable.
COPY --from=builder /go/bin/server /server

# NB: this pulls directly from the upstream image, which already has ca-certificates:
COPY --from=alpine:latest /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Run the server binary.
ENTRYPOINT ["/server"]