package main

import (
	"context"
	"log"
	"os"
	"time"

	pb "grpc-demo/example"

	"google.golang.org/grpc"
)

const (
	address   = "localhost:50051"
	defaultId = "A12345"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	// Create a new connection
	// This can be reused
	client := pb.NewExampleClient(conn)

	// Contact the server and print out its response.
	id := defaultId
	if len(os.Args) > 1 {
		id = os.Args[1]
	}

	// Define execution context to run in the background with a timeout of 1 second
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Perform the function call
	r, err := client.Get(ctx, &pb.GetRequest{Id: id})
	if err != nil {
		log.Fatalf("could not get: %v", err)
	}

	// Use the result
	log.Printf("Get Response: %s for %s", r.GetMessage(), r.GetId())
}
