0. Install golang  

1. Create new golang project    
```bash
mkdir -p grpc-demo
cd grpc-demo
go mod init grpc-demo
```
  

2. Go get the needed dependancies
```bash
go get google.golang.org/grpc
go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
go get google.golang.org/protobuf
```


3. Write a .proto file that describes your service and message shapes
```proto3
syntax = "proto3";
option go_package = "example/example"; // where to put the code
package example; // what the golang package name will be for importing

service Example {
    rpc Get (GetRequest) returns (GetResponse) {}
}

message GetRequest {
    string id = 1;
}

message GetResponse {
    string id = 1;
    string message = 2;
}
```

4. Get protoc compiler
```bash
brew install protobuf
```

5. Install golang plugins for protoc
```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
```

6. Run protoc to generate code
```bash
protoc \
   --go_out=example \
   --go_opt=paths=source_relative \
   --go-grpc_out=example \
   --go-grpc_opt=paths=source_relative \
   example.proto
```

7. Write some code that uses the generated .go files for your service 
```go
package main

import (
	"context"
	"log"
	"net"

	pb "grpc-demo/example"

	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// server is used to implement example.ExampleServer
type exampleServer struct {
	pb.UnimplementedExampleServer
}

// Get implements example.Get
// You can implement this however your business logic needs
// Go get stuff from databases, process data, make other request, ect
func (s *exampleServer) Get(ctx context.Context, request *pb.GetRequest) (*pb.GetResponse, error) {
	log.Printf("Got Request for ID: %s\n", request.GetId())
	return &pb.GetResponse{Id: request.GetId(), Message: "Hello Get!"}, nil
}

func main() {
	// Create tcp listener on port 50051
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create server instance and register it as a gRPC server
	server := grpc.NewServer()
	pb.RegisterExampleServer(server, &exampleServer{})

	// Serve gRPC server over tcp listener
	if err := server.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

``` 

8. Profit