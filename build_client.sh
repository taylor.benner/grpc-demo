#!/bin/bash

cd client && \
    go build -o ~/Programming/lab/grpc-demo/example_client && \
    cd ../;