#!/bin/bash

docker rm -f grpc-demo

docker run \
    --name=grpc-demo \
    -p 50051:50051 \
    -d -t grpc-demo