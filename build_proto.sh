#!/bin/bash
mkdir -p ./example/

protoc \
   --go_out=example \
   --go_opt=paths=source_relative \
   --go-grpc_out=example \
   --go-grpc_opt=paths=source_relative \
   example.proto